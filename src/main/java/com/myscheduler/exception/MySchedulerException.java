package com.myscheduler.exception;

public class MySchedulerException extends RuntimeException {

    public MySchedulerException(String message) {
        super(message);
    }
}
