package com.myscheduler.service;

import com.myscheduler.model.Shift;

import java.util.Collection;

public interface ShiftService {

    Shift create(Shift shift);

    Collection<Shift> list();

    Shift findById(Long id);

    Shift update(Shift shift);

    void delete(Long id);
}
