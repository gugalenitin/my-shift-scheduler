package com.myscheduler.service;

import com.myscheduler.model.Worker;

import java.util.Collection;

public interface WorkerService {

    Collection<Worker> list();

    Worker findById(Long id);

    void delete(Long id);

    Worker assignToShift(Long workerId, Long shiftId);
}
