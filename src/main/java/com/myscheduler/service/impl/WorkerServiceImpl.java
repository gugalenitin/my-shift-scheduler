package com.myscheduler.service.impl;

import com.myscheduler.exception.MySchedulerException;
import com.myscheduler.model.Shift;
import com.myscheduler.model.Worker;
import com.myscheduler.service.ShiftService;
import com.myscheduler.service.WorkerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class WorkerServiceImpl implements WorkerService {

    private Map<Long, Worker> workers;

    private ShiftService shiftService;

    @Autowired
    public WorkerServiceImpl(ShiftService shiftService) {
        workers = new LinkedHashMap<>();
        this.shiftService = shiftService;
    }

    @Override
    public Collection<Worker> list() {
        return workers.values();
    }

    @Override
    public Worker findById(Long id) {
        if (!workers.containsKey(id))
            throw new MySchedulerException(String.format("Worker with id %d does not exist!", id));
        return workers.get(id);
    }

    @Override
    public void delete(Long id) {
        // Not throwing exception here as it is delete operation
        workers.remove(id);
    }

    @Override
    public Worker assignToShift(Long workerId, Long shiftId) {
        // Check if shift is present
        Shift shift = shiftService.findById(shiftId);
        Worker worker = workers.get(workerId);

        // Create Worker with given shift if not present already
        if (worker == null) {
            createWorker(workerId, shift);
        } else {
            addWorkerShift(worker, shift);
        }
        return workers.get(workerId);
    }

    private void addWorkerShift(Worker worker, Shift shift) {
        validateWorkerShift(worker, shift);
        worker.getShifts().add(shift);
    }

    private void validateWorkerShift(Worker worker, Shift shift) {
        for (Shift workerShift : worker.getShifts()) {
            if (isSameShift(shift, workerShift)) {
                throw new MySchedulerException("Worker is already in the same shift");
            }
            if (isShiftInRow(shift, workerShift)) {
                throw new MySchedulerException(String.format("Shift %d comes immediately before/after shift %s",
                        shift.getId(), workerShift));
            }
            if (isOverlappingShift(shift, workerShift)) {
                throw new MySchedulerException(String.format("Shift %d is overlapping with shift %s",
                        shift.getId(), workerShift));
            }
        }
    }

    private boolean isSameShift(Shift shift, Shift workerShift) {
        return shift.getId().equals(workerShift.getId());
    }

    private boolean isShiftInRow(Shift shift, Shift workerShift) {
        return shift.getStartDate().equals(workerShift.getEndDate())
                || shift.getEndDate().equals(workerShift.getStartDate());
    }

    private boolean isOverlappingShift(Shift shift, Shift workerShift) {
        return isBetween(shift.getStartDate(), workerShift)
                || isBetween(shift.getEndDate(), workerShift);
    }

    private boolean isBetween(Date date, Shift shift) {
        return date.after(shift.getStartDate()) && date.before(shift.getEndDate());
    }

    private void createWorker(Long workerId, Shift shift) {
        Worker worker = new Worker();
        worker.setId(workerId);
        worker.setShifts(new LinkedHashSet<>());
        worker.getShifts().add(shift);
        workers.put(workerId, worker);
    }
}
