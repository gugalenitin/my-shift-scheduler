package com.myscheduler.service.impl;

import com.myscheduler.exception.MySchedulerException;
import com.myscheduler.model.Shift;
import com.myscheduler.service.ShiftService;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ShiftServiceImpl implements ShiftService {

    private Map<Long, Shift> shifts;

    public ShiftServiceImpl() {
        shifts = new LinkedHashMap<>();
    }

    @Override
    public Shift create(Shift shift) {
        validate(shift);
        // Check for duplicate shift id
        if (shifts.containsKey(shift.getId()))
            throw new MySchedulerException(String.format("Shift with id %d already exists!", shift.getId()));
        shifts.put(shift.getId(), shift);
        return shift;
    }

    private void validate(Shift shift) {
        if (shift.getId() == null || shift.getStartDate() == null || shift.getEndDate() == null)
            throw new MySchedulerException(String.format("Invalid shift. All values should be present. id %d," +
                    " startDate %s, endDate %s", shift.getId(), shift.getStartDate(), shift.getEndDate()));
        if (shift.getStartDate().equals(shift.getEndDate())
                || shift.getStartDate().after(shift.getEndDate()))
            throw new MySchedulerException(String.format("Invalid shift. startDate %s should come before endDate %s",
                    shift.getStartDate(), shift.getEndDate()));
        Long shiftId = isDuplicateShift(shift);
        if (shiftId != null) {
            throw new MySchedulerException(String.format("Shift with id %d is already present with same start and end dates!",
                    shiftId));
        }
    }

    private Long isDuplicateShift(Shift shift) {
        for (Shift availShift: shifts.values()) {
            if (shift.getStartDate().equals(availShift.getStartDate())
                    && shift.getEndDate().equals(availShift.getEndDate()))
                return availShift.getId();
        }
        return null;
    }

    @Override
    public Collection<Shift> list() {
        return shifts.values();
    }

    @Override
    public Shift findById(Long id) {
        // Check if shift is present
        if (!shifts.containsKey(id))
            throw new MySchedulerException(String.format("Shift with id %d does not exist!", id));
        return shifts.get(id);
    }

    @Override
    public Shift update(Shift shift) {
        validate(shift);
        // Check if shift is present
        findById(shift.getId());
        shifts.put(shift.getId(), shift);
        return shift;
    }

    @Override
    public void delete(Long id) {
        // Not throwing exception here as it is delete operation
        shifts.remove(id);
    }
}
