package com.myscheduler.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class Shift {
    private Long id;
    private Date startDate;
    private Date endDate;

    @Override
    public String toString() {
        return "Shift{" +
                "id=" + id +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                '}';
    }
}
