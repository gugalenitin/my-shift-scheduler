package com.myscheduler.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
public class Worker {
    private Long id;
    private Set<Shift> shifts;

    @Override
    public String toString() {
        return "Worker{" +
                "id=" + id +
                ", shifts=" + shifts +
                '}';
    }
}
