package com.myscheduler.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ShiftDto {
    private Long id;
    private String startDate;
    private String endDate;
}
