package com.myscheduler.converter;

import com.myscheduler.dto.ShiftDto;
import com.myscheduler.dto.WorkerDto;
import com.myscheduler.exception.MySchedulerException;
import com.myscheduler.model.Shift;
import com.myscheduler.model.Worker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.stream.Collectors;

@Component
public class WorkerDtoConverter {

    public static SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat("dd-MM-yyyy HH:mm");

    @Autowired
    private ShiftDtoConverter shiftDtoConverter;

    public WorkerDto toDto(Worker model) {
        WorkerDto dto = new WorkerDto();
        dto.setId(model.getId());
        dto.setShifts(model.getShifts().stream().map(shiftDtoConverter::toDto).collect(Collectors.toSet()));
        return dto;
    }

}
