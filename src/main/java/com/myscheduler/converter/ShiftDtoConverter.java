package com.myscheduler.converter;

import com.myscheduler.dto.ShiftDto;
import com.myscheduler.exception.MySchedulerException;
import com.myscheduler.model.Shift;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;

@Component
public class ShiftDtoConverter {

    public static SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat("dd-MM-yyyy HH:mm");

    public Shift toModel(ShiftDto dto) {
        if (dto.getId() == null || dto.getStartDate() == null || dto.getEndDate() == null)
            throw new MySchedulerException(String.format("Invalid shift. All values should be present. id %d," +
                    " startDate %s, endDate %s", dto.getId(), dto.getStartDate(), dto.getEndDate()));
        Shift model = new Shift();
        try {
            model.setId(dto.getId());
            model.setStartDate(DATE_FORMATTER.parse(dto.getStartDate()));
            model.setEndDate(DATE_FORMATTER.parse(dto.getEndDate()));
        } catch (ParseException ex) {
            throw new MySchedulerException("Date should be in 'dd-MM-yyyy HH:mm' format. Example: 20-11-2021 12:00");
        }
        return model;
    }

    public ShiftDto toDto(Shift model) {
        ShiftDto dto = new ShiftDto();
        dto.setId(model.getId());
        dto.setStartDate(DATE_FORMATTER.format(model.getStartDate()));
        dto.setEndDate(DATE_FORMATTER.format(model.getEndDate()));
        return dto;
    }

}
