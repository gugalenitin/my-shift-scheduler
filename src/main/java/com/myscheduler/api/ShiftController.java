package com.myscheduler.api;

import com.myscheduler.converter.ShiftDtoConverter;
import com.myscheduler.dto.ShiftDto;
import com.myscheduler.model.Shift;
import com.myscheduler.service.ShiftService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/shifts")
public class ShiftController {
    @Autowired
    private ShiftService shiftService;

    @Autowired
    private ShiftDtoConverter dtoConverter;

    @PostMapping
    public ShiftDto create(@RequestBody ShiftDto shift) {
        return dtoConverter.toDto(shiftService.create(dtoConverter.toModel(shift)));
    }

    @GetMapping
    public Collection<ShiftDto> list() {
        Collection<Shift> shifts = shiftService.list();
        return shifts.stream().map(dtoConverter::toDto).collect(Collectors.toList());
    }

    @GetMapping("/{shiftId}")
    public ShiftDto get(@PathVariable Long shiftId) {
        return dtoConverter.toDto(shiftService.findById(shiftId));
    }

    @PutMapping
    public ShiftDto update(@RequestBody ShiftDto shift) {
        return dtoConverter.toDto(shiftService.update(dtoConverter.toModel(shift)));
    }

    @DeleteMapping("/{shiftId}")
    public void delete(@PathVariable Long shiftId) {
        shiftService.delete(shiftId);
    }

}
