package com.myscheduler.api;

import com.myscheduler.converter.WorkerDtoConverter;
import com.myscheduler.dto.WorkerDto;
import com.myscheduler.model.Worker;
import com.myscheduler.service.WorkerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/workers")
public class WorkerController {
    @Autowired
    private WorkerService workerService;

    @Autowired
    private WorkerDtoConverter dtoConverter;

    @GetMapping
    public Collection<WorkerDto> list() {
        Collection<Worker> workers = workerService.list();
        return workers.stream().map(dtoConverter::toDto).collect(Collectors.toList());
    }

    @GetMapping("/{workerId}")
    public WorkerDto get(@PathVariable Long workerId) {
        return dtoConverter.toDto(workerService.findById(workerId));
    }

    @DeleteMapping("/{workerId}")
    public void delete(@PathVariable Long workerId) {
        workerService.delete(workerId);
    }

    @PostMapping("/{workerId}/assign-shift")
    public WorkerDto assignToShift(@PathVariable Long workerId, @RequestParam("shiftId") Long shiftId) {
        return dtoConverter.toDto(workerService.assignToShift(workerId, shiftId));
    }

}
