package com.myscheduler.service;

import com.myscheduler.exception.MySchedulerException;
import com.myscheduler.service.impl.ShiftServiceImpl;
import com.myscheduler.util.ShiftMock;
import com.myscheduler.model.Shift;
import org.junit.Before;
import org.junit.Test;

import java.util.Collection;

import static org.junit.Assert.*;

public class ShiftServiceTest {
    private ShiftService shiftService;

    @Before
    public void init() {
        shiftService = new ShiftServiceImpl();
    }

    @Test
    public void create() throws Exception {
        Shift shift = ShiftMock.getShift(1, "1-3-2021 0:0", "1-3-2021 8:0");

        Shift shiftResult = shiftService.create(shift);

        ShiftMock.assertShift(shift, shiftResult);
    }

    @Test(expected = MySchedulerException.class)
    public void createWithNull() throws Exception {
        shiftService.create(new Shift());
    }

    @Test(expected = MySchedulerException.class)
    public void createWithSameId() throws Exception {
        Shift shift = ShiftMock.getShift(1, "1-3-2021 0:0", "1-3-2021 8:0");
        shiftService.create(shift);

        shift = ShiftMock.getShift(1, "1-3-2021 8:0", "1-3-2021 16:0");
        shiftService.create(shift);
    }

    @Test
    public void createWithInvalidDate() throws Exception {
        Shift shift = ShiftMock.getShift(1, "1-3-2021 0:0", "1-3-2021 8:0");
        shiftService.create(shift);

        try {
            shift = ShiftMock.getShift(2, "1-3-2021 8:0", "1-3-2021 0:0");
            shiftService.create(shift);
            fail("Should fail to create shift with end_date before start_date.");
        } catch (MySchedulerException ex) {
            // NOOP
        }

        try {
            shift = ShiftMock.getShift(2, "1-3-2021 0:0", "1-3-2021 0:0");
            shiftService.create(shift);
            fail("Should fail to create shift with same start and end dates.");
        } catch (MySchedulerException ex) {
            // NOOP
        }

        try {
            shift = ShiftMock.getShift(2, "1-3-2021 0:0", "1-3-2021 8:0");
            shiftService.create(shift);
            fail("Should fail to create shift with duplicate start and end dates.");
        } catch (MySchedulerException ex) {
            // NOOP
        }
    }

    @Test
    public void list() throws Exception {
        shiftService.create(ShiftMock.getShift(1, "1-3-2021 0:0", "1-3-2021 8:0"));
        shiftService.create(ShiftMock.getShift(2, "1-3-2021 8:0", "1-3-2021 16:0"));
        shiftService.create(ShiftMock.getShift(3, "1-3-2021 16:0", "2-3-2021 0:0"));

        Collection<Shift> shifts = shiftService.list();

        assertFalse(shifts.isEmpty());
        assertEquals(3, shifts.size());
    }

    @Test
    public void findById() throws Exception {
        Shift shift = ShiftMock.getShift(1, "1-3-2021 0:0", "1-3-2021 8:0");

        shiftService.create(shift);
        Shift shiftResult = shiftService.findById(1L);

        ShiftMock.assertShift(shift, shiftResult);
    }

    @Test(expected = MySchedulerException.class)
    public void findByIdNegative() throws Exception {
        Shift shift = ShiftMock.getShift(1, "1-3-2021 0:0", "1-3-2021 8:0");

        shiftService.create(shift);
        shiftService.findById(2L);
    }

    @Test
    public void update() throws Exception {
        Shift shift = ShiftMock.getShift(1, "1-3-2021 0:0", "1-3-2021 8:0");
        shiftService.create(shift);

        shift = ShiftMock.getShift(1, "2-3-2021 0:0", "2-3-2021 8:0");
        Shift shiftResult = shiftService.update(shift);

        ShiftMock.assertShift(shift, shiftResult);
    }

    @Test(expected = MySchedulerException.class)
    public void updateWithNull() throws Exception {
        shiftService.update(new Shift());
    }

    @Test(expected = MySchedulerException.class)
    public void updateWithInvalidId() throws Exception {
        Shift shift = ShiftMock.getShift(1, "1-3-2021 0:0", "1-3-2021 8:0");
        shiftService.create(shift);

        shift = ShiftMock.getShift(2, "2-3-2021 0:0", "2-3-2021 8:0");
        shiftService.update(shift);
    }

    @Test
    public void updateWithInvalidDate() throws Exception {
        Shift shift = ShiftMock.getShift(2, "1-3-2021 0:0", "1-3-2021 8:0");
        shiftService.create(shift);

        try {
            shift = ShiftMock.getShift(2, "1-3-2021 8:0", "1-3-2021 0:0");
            shiftService.update(shift);
            fail("Should fail to create shift with end_date before start_date.");
        } catch (MySchedulerException ex) {
            // NOOP
        }

        try {
            shift = ShiftMock.getShift(2, "1-3-2021 0:0", "1-3-2021 0:0");
            shiftService.update(shift);
            fail("Should fail to create shift with same start and end dates.");
        } catch (MySchedulerException ex) {
            // NOOP
        }

        try {
            shift = ShiftMock.getShift(2, "1-3-2021 0:0", "1-3-2021 8:0");
            shiftService.update(shift);
            fail("Should fail to create shift with duplicate start and end dates.");
        } catch (MySchedulerException ex) {
            // NOOP
        }
    }

    @Test
    public void delete() throws Exception {
        Shift shift = ShiftMock.getShift(1, "1-3-2021 0:0", "1-3-2021 8:0");
        shiftService.create(shift);

        assertNotNull(shiftService.findById(1L));

        shiftService.delete(1L);

        try {
            shiftService.findById(1L);
            fail("Should fail to find shift with id 1.");
        } catch (MySchedulerException ex) {
            // NOOP
        }
    }

}
