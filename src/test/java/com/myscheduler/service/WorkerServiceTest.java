package com.myscheduler.service;

import com.myscheduler.exception.MySchedulerException;
import com.myscheduler.model.Worker;
import com.myscheduler.service.impl.ShiftServiceImpl;
import com.myscheduler.service.impl.WorkerServiceImpl;
import com.myscheduler.util.ShiftMock;
import org.junit.Before;
import org.junit.Test;

import java.util.Collection;

import static org.junit.Assert.*;

public class WorkerServiceTest {
    private WorkerService workerService;

    @Before
    public void init() throws Exception {
        ShiftService shiftService = new ShiftServiceImpl();
        workerService = new WorkerServiceImpl(shiftService);
        shiftService.create(ShiftMock.getShift(1, "1-3-2021 0:0", "1-3-2021 8:0"));
        shiftService.create(ShiftMock.getShift(2, "1-3-2021 8:0", "1-3-2021 16:0"));
        shiftService.create(ShiftMock.getShift(3, "1-3-2021 16:0", "2-3-2021 0:0"));
        shiftService.create(ShiftMock.getShift(4, "2-3-2021 0:0", "2-3-2021 8:0"));
        shiftService.create(ShiftMock.getShift(5, "2-3-2021 8:0", "2-3-2021 16:0"));
        shiftService.create(ShiftMock.getShift(6, "2-3-2021 16:0", "3-3-2021 0:0"));

        // Overlapping shifts
        shiftService.create(ShiftMock.getShift(7, "1-3-2021 8:0", "1-3-2021 12:0"));
        shiftService.create(ShiftMock.getShift(8, "1-3-2021 12:0", "1-3-2021 16:0"));
        shiftService.create(ShiftMock.getShift(9, "1-3-2021 4:0", "1-3-2021 12:0"));
        shiftService.create(ShiftMock.getShift(10, "1-3-2021 12:0", "1-3-2021 20:0"));
    }

    @Test
    public void assignToShift() throws Exception {
        Worker worker = workerService.assignToShift(1L, 1L);

        assertNotNull(worker);
        assertEquals(1, worker.getId().longValue());
        assertEquals(1, worker.getShifts().size());

        worker = workerService.assignToShift(1L, 3L);

        assertEquals(2, worker.getShifts().size());

        worker = workerService.assignToShift(1L, 5L);

        assertEquals(3, worker.getShifts().size());
    }

    @Test(expected = MySchedulerException.class)
    public void assignToShiftShiftNotPresent() throws Exception {
        workerService.assignToShift(1L, 11L);
    }

    @Test(expected = MySchedulerException.class)
    public void assignToSameShift() throws Exception {
        workerService.assignToShift(1L, 1L);
        workerService.assignToShift(1L, 1L);
    }

    @Test
    public void assignToShiftInRow() throws Exception {
        workerService.assignToShift(1L, 2L);

        try {
            workerService.assignToShift(1L, 1L);
            fail("Should fail to assign a shift in row.");
        } catch (MySchedulerException ex) {
            // NOOP
        }

        try {
            workerService.assignToShift(1L, 3L);
            fail("Should fail to assign a shift in row.");
        } catch (MySchedulerException ex) {
            // NOOP
        }
    }

    @Test
    public void assignToOverLappingShift() throws Exception {
        workerService.assignToShift(1L, 2L);

        try {
            workerService.assignToShift(1L, 7L);
            fail("Should fail to assign a overlapping shift.");
        } catch (MySchedulerException ex) {
            // NOOP
        }

        try {
            workerService.assignToShift(1L, 8L);
            fail("Should fail to assign a overlapping shift.");
        } catch (MySchedulerException ex) {
            // NOOP
        }

        try {
            workerService.assignToShift(1L, 9L);
            fail("Should fail to assign a overlapping shift.");
        } catch (MySchedulerException ex) {
            // NOOP
        }

        try {
            workerService.assignToShift(1L, 10L);
            fail("Should fail to assign a overlapping shift.");
        } catch (MySchedulerException ex) {
            // NOOP
        }
    }

    @Test
    public void list() throws Exception {
        workerService.assignToShift(1L, 1L);
        workerService.assignToShift(2L, 1L);
        workerService.assignToShift(3L, 1L);
        workerService.assignToShift(1L, 3L);
        workerService.assignToShift(2L, 4L);
        workerService.assignToShift(3L, 5L);

        Collection<Worker> workers = workerService.list();

        assertFalse(workers.isEmpty());
        assertEquals(3, workers.size());
    }

    @Test
    public void findById() throws Exception {
        workerService.assignToShift(1L, 1L);
        Worker worker = workerService.findById(1L);

        assertNotNull(worker);
        assertEquals(1, worker.getId().longValue());
        assertEquals(1, worker.getShifts().size());
    }

    @Test(expected = MySchedulerException.class)
    public void findByIdNegative() throws Exception {
        workerService.assignToShift(1L, 1L);
        workerService.findById(2L);
    }

    @Test
    public void delete() throws Exception {
        workerService.assignToShift(1L, 1L);

        assertNotNull(workerService.findById(1L));

        workerService.delete(1L);

        try {
            workerService.findById(1L);
            fail("Should fail to find worker with id 1.");
        } catch (MySchedulerException ex) {
            // NOOP
        }
    }

}
