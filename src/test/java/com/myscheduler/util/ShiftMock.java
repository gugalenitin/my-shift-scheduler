package com.myscheduler.util;

import com.myscheduler.converter.ShiftDtoConverter;
import com.myscheduler.model.Shift;

import java.text.ParseException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ShiftMock {

    public static Shift getShift(long id, String startDate, String endDate) throws ParseException {
        Shift shift = new Shift();
        shift.setId(id);
        shift.setStartDate(ShiftDtoConverter.DATE_FORMATTER.parse(startDate));
        shift.setEndDate(ShiftDtoConverter.DATE_FORMATTER.parse(endDate));
        return shift;
    }

    public static void assertShift(Shift expectedShift, Shift actualShift) {
        assertNotNull(actualShift);
        assertEquals(expectedShift.getId().longValue(), actualShift.getId().longValue());
        assertEquals(expectedShift.getStartDate(), actualShift.getStartDate());
        assertEquals(expectedShift.getEndDate(), actualShift.getEndDate());
    }

}
