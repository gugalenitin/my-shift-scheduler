# Spring Boot Shift Scheduler RESTful Application #
## 1.0 ##

This app is based on Spring Boot Web application. It uses ```spring-boot-starter-web``` and ```spring-boot-starter-test``` starter libs.

## Requirements ##

* Java 8
* Maven

## How to run? ##

* Run API - ```mvn spring-boot:run```
* Run Tests - ```mvn clean test```

## API Docs ##

* Create shift - ```/api/v1/shifts - POST```

Sample request data -

```
#!json

{
    "id" : 1,
    "startDate" : "1-3-2021 8:0",
    "endDate" : "2-3-2021 16:0"
}
```


Sample response -

```
#!json

{
    "id" : 1,
    "startDate" : "1-3-2021 8:0",
    "endDate" : "2-3-2021 16:0"
}
```


* Get shifts - ```/api/v1/shifts - GET```

Sample response -

```
#!json

[
    {
        "id": 1,
        "startDate": "01-03-2021 08:00",
        "endDate": "02-03-2021 16:00"
    },
    {
        "id": 2,
        "startDate": "01-03-2021 16:00",
        "endDate": "03-03-2021 00:00"
    }
]
```


* Update shift - ```/api/v1/shifts - PUT```

Sample request data -

```
#!json

{
    "id" : 1,
    "startDate" : "1-3-2021 8:0",
    "endDate" : "2-3-2021 16:0"
}
```

Sample response -

```
#!json

{
    "id" : 1,
    "startDate" : "1-3-2021 8:0",
    "endDate" : "2-3-2021 16:0"
}
```


* Get shift - ```/api/v1/shifts/<id> - GET```

Sample request - ```/api/v1/shifts/1```

Sample response -

```
#!json

{
    "id" : 1,
    "startDate" : "1-3-2021 8:0",
    "endDate" : "2-3-2021 16:0"
}
```


* Delete shift - ```/api/v1/shifts/<id> - DELETE```

Sample request - ```/api/v1/shifts/1```

Sample response - ```<no response>```


* Assign worker to shift - ```/api/v1/workers/<id>/assign-shift?shiftId=<shift-id> - POST```

Sample request - ```/api/v1/workers/1/assign-shift?shiftId=2```

Sample response -

```
#!json

{
    "id": 1,
    "shifts": [
        {
            "id": 2,
            "startDate": "01-03-2021 16:00",
            "endDate": "03-03-2021 00:00"
        }
    ]
}
```

* Get workers - ```/api/v1/workers - GET```

Sample request - ```/api/v1/workers```

Sample response -

```
#!json

[
    {
        "id": 1,
        "shifts": [
            {
                "id": 2,
                "startDate": "01-03-2021 16:00",
                "endDate": "03-03-2021 00:00"
            }
        ]
    },
    {
        "id": 2,
        "shifts": [
            {
                "id": 2,
                "startDate": "01-03-2021 16:00",
                "endDate": "03-03-2021 00:00"
            }
        ]
    }
]
```

* Get worker - ```/api/v1/workers/<id> - GET```

Sample request - ```/api/v1/workers/1/```

Sample response -

```
#!json

{
    "id": 1,
    "shifts": [
        {
            "id": 2,
            "startDate": "01-03-2021 16:00",
            "endDate": "03-03-2021 00:00"
        }
    ]
}
```

* Delete worker - ```/api/v1/workers/<id> - DELETE```

Sample request - ```/api/v1/workers/1```

Sample response - ```<no response>```